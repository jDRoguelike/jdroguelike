package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import model.Weapon;

public class WeaponSprite extends Sprite implements Item {

    private Texture playerAttackSprite;
    private String name;
    private Weapon weaponStats;

    public WeaponSprite(Texture texture, Texture playerAttackSprite, int x, int y, int height, int width, Weapon weapon) {
        super(texture);
        this.playerAttackSprite = playerAttackSprite;
        this.setOrigin(0, 0);
        this.setSize(width, height);
        this.setPosition(x, y);
        this.setWeaponStats(weapon);
    }

    public Texture getPlayerAttackSprite() {
        return playerAttackSprite;
    }

    public void setPlayerAttackSprite(Texture playerAttackSprite) {
        this.playerAttackSprite = playerAttackSprite;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean collision(Rectangle boundingBox) {
        if (boundingBox.overlaps(this.getBoundingRectangle())) {
            return true;
        }
        return false;
    }

    @Override
    public void applyItem(PlayerSprite player) {
        player.getStats().setWeapon(this.getWeaponStats());
        player.setWeaponSprite(this);
    }

    public void dropWeapon(float x, float y, Vector2 direction) {
        //here you would run the animation for dropping the weapon, but for now we will just place it
        //away from the player sprite slightly
        this.setPosition(x + (20 * direction.x), y + (20 * direction.y));
    }

    public Weapon getWeaponStats() {
        return weaponStats;
    }

    public void setWeaponStats(Weapon weaponStats) {
        this.weaponStats = weaponStats;
    }
}
