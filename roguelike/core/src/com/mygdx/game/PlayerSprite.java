package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import model.Player;

public class PlayerSprite extends Sprite {

    private Player stats;
    private WeaponSprite weaponSprite;
    private Vector2 center;

    public PlayerSprite(Texture texture, Player stats, WeaponSprite weaponSprite, int x, int y, int width, int height) {
        super(texture);
        float aspectRatio = (float)texture.getWidth() / (float)texture.getHeight();
        this.setSize(width * aspectRatio, height);
        this.setOrigin(this.getWidth() / 2, this.getHeight() /2);
        this.setPosition(x, y);
        this.stats = stats;
        this.weaponSprite = weaponSprite;
        center = new Vector2(this.getOriginX(), this.getOriginY());
    }

    public Vector2 getCenter() {
        center.x = this.getX() + (this.getWidth() / 2);
        center.y = this.getY() + (this.getHeight() / 2);
        return center;
    }

    public Vector2 getCenterRotated() {
        center.x = this.getX() + (this.getWidth() / 2);
        center.y = this.getY() + (this.getHeight() / 2);
        return center.cpy().rotate(this.getRotation());
    }


    public Player getStats() {
        return stats;
    }

    public void setStats(Player stats) {
        this.stats = stats;
    }

    public WeaponSprite getWeaponSprite() {
        return weaponSprite;
    }

    public void setWeaponSprite(WeaponSprite weaponSprite) {
        this.weaponSprite = weaponSprite;
    }
}
