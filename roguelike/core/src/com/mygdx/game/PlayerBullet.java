package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import model.Player;
import model.RangedWeapon;

public class PlayerBullet implements BulletObject {

    private boolean spawned = false;
    private Texture texture;
    private Sprite bulletSprite;
    private Vector2 direction;
    private int speed = 100; // world unites per second
    private int maxDistance = 1200;
    private int damage;
    //these we don't need a getter or setter for, it's internal only
    private Vector2 velocity = new Vector2(0,0);
    public float distanceTravelled;
    private Vector2 position;

    //for now the player import only reflects damage but there is definitely more we want to pull from the player here
    PlayerBullet(float startingX, float startingY, float targetX, float targetY, Texture startingTexture, Player player) {
        this.setDamage(player.determineDamageOutput());
        position = new Vector2(startingX, startingY);
        direction = new Vector2(targetX - startingX, targetY - startingY);
        this.setBulletSprite(new Sprite(startingTexture));
        this.getBulletSprite().setSize(2,2);
        this.getBulletSprite().setOrigin(this.getBulletSprite().getWidth() / 2, this.getBulletSprite().getHeight() / 2);
        this.getBulletSprite().setPosition(position.x, position.y);
        this.setSpeed(((RangedWeapon)player.getWeapon()).getFlightSpeed());
        this.setMaxDistance(((RangedWeapon)player.getWeapon()).getFlightDistance());
    }

    //region getters and setters

    @Override
    public void setSpawned(boolean spawned) {
        this.spawned = spawned;
    }

    @Override
    public boolean isSpawned() {
        return spawned;
    }

    @Override
    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public void setBulletSprite(Sprite bulletSprite) {
        this.bulletSprite = bulletSprite;
    }

    @Override
    public Sprite getBulletSprite() {
        return bulletSprite;
    }

    @Override
    public void setDirection(Vector2 direction) {
        this.direction = direction;
    }

    @Override
    public Vector2 getDirection() {
        return direction;
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    //endregion

    @Override
    public void kill(BulletPool pool) {
        pool.getPlayerBullets().remove(this);
    }

    @Override
    public void update(float delta, Map map) {
        direction.nor();
        velocity = direction.cpy().scl(speed * delta);

        if (map.collision(velocity, position, this.getBulletSprite().getWidth(), this.getBulletSprite().getHeight())) {
            spawned = false;
        }

        position.add(velocity);
        distanceTravelled += velocity.len();
        getBulletSprite().setPosition(position.x, position.y);
    }

    @Override
    public void draw(SpriteBatch sb) {
        getBulletSprite().draw(sb);
    }

    @Override
    public boolean collision(Rectangle boundingBox) {
        return boundingBox.overlaps(bulletSprite.getBoundingRectangle());
    }
}
