package com.mygdx.game;

import java.util.ArrayList;

public class EnemyPool {

    private ArrayList<EnemySprite> enemySprites;

    public EnemyPool() {
        enemySprites = new ArrayList<>();
    }

    public void addEnemySpite(EnemySprite enemySprite) {
        enemySprites.add(enemySprite);
    }

    public void removeEnemySprite(EnemySprite enemySprite) {
        enemySprites.remove(enemySprite);
    }

    public ArrayList<EnemySprite> getEnemySprites() {
        return enemySprites;
    }
}
