package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import model.Boss;

import java.util.Arrays;

public class EnemySprite extends Sprite {

    private Boss stats;

    public EnemySprite(Texture texture, int x, int y, int height, int width, String name, int health, int level, int armour, int power, int speed) {
        super(texture);
        this.setSize(width, height);
        this.setOrigin(this.getWidth() / 2, this.getHeight() / 2);
        this.setPosition(x, y);
        stats = new Boss(name, health, level, armour, power, speed);
    }

    public Boss getStats() {
        return stats;
    }

    public void setStats(Boss stats) {
        this.stats = stats;
    }

    public boolean isDead() {
        return this.stats.getHealth() <= 0;
    }
}
