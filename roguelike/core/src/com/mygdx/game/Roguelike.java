package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;
import javafx.scene.shape.Polygon;
import model.MeleeWeapon;
import model.Player;
import model.RangedWeapon;
import org.w3c.dom.css.Rect;

import java.util.ArrayList;

public class Roguelike extends ApplicationAdapter implements InputProcessor {

	AssetManager assetManager = new AssetManager();
	SpriteBatch batch;
	SpriteBatch fontBatch;
	PlayerSprite playerCharacter;
	WeaponSprite weaponSprite;
	WeaponSprite weaponSprite2;
	WeaponSprite weaponSprite3;
	WeaponSprite weaponMelee;
	Cone meleeAttack;
	BulletPool bulletPool;
	EnemyPool enemyPool;
	ItemPool weaponPool;
	BitmapFont font;
	OrthographicCamera camera;
	OrthographicCamera fontCamera;
	private float WORLDHEIGHT = 100, WORLDWIDTH = 100;
	private Map currentMap;
	Vector2 position = new Vector2(30,30);
	Vector2 velocity = new Vector2(0 ,0);
	Vector2 direction = new Vector2(0 ,0);
	Vector2 mousePosition = new Vector2(0, 0);
	private boolean isAttacking = false;
	private boolean isDashing = false;
	private float timeSinceLastAttackSeconds = 0;
	private float dashingDistance = 0;

	private Vector3 getMousePosInGameWorld() {
		return camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
	}

	private Vector3 getScreenPosFromGameWorld(float camX, float camY) {
		return camera.project(new Vector3(camX, camY, 0));
	}

	private Vector3 getWorldPositionFromScreenPosition(float screenX, float screenY) {
		return camera.unproject(new Vector3(screenX, screenY, 0));
	}

	//an attackspeed of 1 will give one attack per second, faster will produce faster attack speeds
	private void attackConstantly() {
		if (timeSinceLastAttackSeconds > 1f / playerCharacter.getStats().getWeapon().getAtkspeed()) {
			if(playerCharacter.getStats().getWeapon().getClass() == RangedWeapon.class)
				bulletPool.addPlayerBullet(mousePosition.x, mousePosition.y, playerCharacter);
			else {
				meleeAttack();
			}
			timeSinceLastAttackSeconds = 0;
		}
	}

	private void meleeAttack() {
		for (EnemySprite enemy : enemyPool.getEnemySprites()) {
			//firstly check the distance from the attack (easiest calculation to break out if false)
			if (meleeAttack.collision(enemy.getBoundingRectangle()))
				enemy.getStats().damage(playerCharacter.getStats().getWeapon().getDamage());
		}
	}

	private void testSubjects() {

		assetManager.load("enemy.png", Texture.class);
		assetManager.load("player.png", Texture.class);
		assetManager.load("playerbullet.png", Texture.class);
		assetManager.load("playerbullet2.png", Texture.class);
		assetManager.load("playerbullet3.png", Texture.class);
		assetManager.load("swordslash.png", Texture.class);
		assetManager.load("weapon.png", Texture.class);
		assetManager.load("weapon2.png", Texture.class);
		assetManager.load("weapon3.png", Texture.class);
		assetManager.load("weaponmelee.png", Texture.class);
		assetManager.load("map1.png", Texture.class);

		assetManager.finishLoading();

		EnemySprite enemy1 = new EnemySprite(assetManager.get("enemy.png", Texture.class),
			80, 50, 10, 10 , "Tutorial Killer Machine",
			100, 1, 1, 2, 1);
		enemyPool.addEnemySpite(enemy1);

		RangedWeapon rangedWeapon = new RangedWeapon("basic weapon", 1, 5, 2, 80, 100);
		weaponSprite = new WeaponSprite(assetManager.get("weapon.png", Texture.class),
			assetManager.get("playerbullet.png", Texture.class),
			0, 0, 2, 2,
			rangedWeapon);
		RangedWeapon rangedWeapon2 = new RangedWeapon("basic weapon 2", 1, 20, 1, 40, 50);
		weaponSprite2 = new WeaponSprite(assetManager.get("weapon2.png", Texture.class),
			assetManager.get("playerbullet2.png", Texture.class),
			20, 20, 2, 2,
			rangedWeapon2);
		RangedWeapon rangedWeapon3 = new RangedWeapon("basic weapon 3", 1, 40, 0.5f, 80, 70);
		weaponSprite3 = new WeaponSprite(assetManager.get("weapon3.png", Texture.class),
			assetManager.get("playerbullet3.png", Texture.class),
			30, 30, 2, 2,
			rangedWeapon3);
		MeleeWeapon weapon4Melee = new MeleeWeapon("basic melee weapon", 1, 10, 3, 20, 2, 10);
		weaponMelee = new WeaponSprite(assetManager.get("weaponmelee.png", Texture.class),
			assetManager.get("swordslash.png", Texture.class),
			40,40, 2, 2,
			weapon4Melee);
		Sprite mapSprite = new Sprite(assetManager.get("map1.png", Texture.class), 0, 0, 125, 125);

		weaponPool.addItemSprite(weaponSprite2);
		weaponPool.addItemSprite(weaponSprite3);
		weaponPool.addItemSprite(weaponMelee);

		Player playerStats = new Player("Jamie",10, 1, 1, 1, 20);
		playerStats.setWeapon(rangedWeapon);
		playerCharacter = new PlayerSprite(assetManager.get("player.png", Texture.class), playerStats, weaponSprite, (int) position.x, (int) position.y, 5, 5);
		meleeAttack = new Cone(5, playerCharacter.getCenter(), 5, playerCharacter.getRotation());

		String[] map = new String[25];
		map[0] = ("0000000000000000000000000");
		for (int i = 1; i < 24; i++) {
			map[i] = ("0111111111111111111111110");
		}
		map[24] = ("0000000000000000000000000");

		currentMap = new Map(mapSprite);
		currentMap.with(map);
	}

	@Override
	public void create () {

		Box2D.init();

		float screenWidth = Gdx.graphics.getWidth(), screenHeight = Gdx.graphics.getHeight();

		batch = new SpriteBatch();
		fontBatch = new SpriteBatch();
		bulletPool = new BulletPool();
		enemyPool = new EnemyPool();
		weaponPool = new ItemPool();
		testSubjects();

		Gdx.input.setInputProcessor(this);

		Gdx.gl.glClearColor(1, 0, 0, 1);

		camera = new OrthographicCamera(WORLDWIDTH, WORLDHEIGHT * (screenHeight / screenWidth));
		camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
		camera.update();

		fontCamera = new OrthographicCamera(screenWidth, screenHeight);
		fontCamera.position.set(fontCamera.viewportWidth / 2f, fontCamera.viewportHeight / 2, 0);
		fontCamera.update();

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Alegreya-Regular.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 16;
		font = generator.generateFont(parameter);
		generator.dispose();
	}

	@Override
	public void render () {
		float delta = Gdx.graphics.getDeltaTime();

		batch.setProjectionMatrix(camera.combined);
		fontBatch.setProjectionMatrix(fontCamera.combined);

		camera.position.set(playerCharacter.getX() + playerCharacter.getWidth(), playerCharacter.getY() + playerCharacter.getHeight(), 0);
		camera.position.x = MathUtils.clamp(camera.position.x, (camera.viewportWidth/2), 125 - (camera.viewportWidth/2));
		camera.position.y = MathUtils.clamp(camera.position.y, (camera.viewportHeight/2), 125 - (camera.viewportHeight/2));
		camera.update();

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (!isDashing) {
			// region handle movement
			//normalise the vector because we only care about direction here
			direction.nor();

			//calculate velocity based on direction and speed - times delta for framerate independence
			velocity = direction.cpy().scl(playerCharacter.getStats().getSpeed() * delta);

			//remove any velocity that we cannot use because we will be walking into a wall
			velocity = currentMap.isWalkable(velocity, position, playerCharacter.getWidth(), playerCharacter.getHeight());

			//add the velocity to the position
			position.add(velocity);

			//update the players position
			playerCharacter.setPosition(position.x, position.y);
			// endregion

			// region handle player direction
			mousePosition.set(getMousePosInGameWorld().x, getMousePosInGameWorld().y);

			float angle = new Vector2(((playerCharacter.getX() + (playerCharacter.getWidth() / 2)) - mousePosition.x),
				((playerCharacter.getY() + (playerCharacter.getHeight() / 2)) - mousePosition.y)).angle();

			// the plus 90 keeps the top of the image pointing the mouse and not the side of the image
			playerCharacter.setRotation(angle + 180);
			// endregion
		} else {
			velocity.x = 100 * delta;
			velocity.y = 0;
			//calculate velocity based on direction and speed - times delta for framerate independence
			velocity.rotate(playerCharacter.getRotation());
			dashingDistance += velocity.len();
			//remove any velocity that we cannot use because we will be walking into a wall
			velocity = currentMap.isWalkable(velocity, position, playerCharacter.getWidth(), playerCharacter.getHeight());
			playerCharacter.setPosition(position.x, position.y);
			position.add(velocity);
			if (dashingDistance >= 20) {
				dashingDistance = 0;
				isDashing = false;
			}
		}

		meleeAttack = new Cone(10, playerCharacter.getCenter(), 90, playerCharacter.getRotation()-90);

		batch.begin();
		currentMap.getMapSprite().draw(batch);
		playerCharacter.draw(batch);

		//region handle bullet drawing and updating

		/*
		it should be noted that we cant use a foreach here because we need to remove from the pool a bullet in the middle
		of the loop
		 */
		for(int i = 0; i < bulletPool.getPlayerBullets().size(); i++) {
			if (bulletPool.getPlayerBullets().get(i).isSpawned()
				&& !(bulletPool.getPlayerBullets().get(i).distanceTravelled > bulletPool.getPlayerBullets().get(i).getMaxDistance())) {
				bulletPool.getPlayerBullets().get(i).update(delta, currentMap);
				bulletPool.getPlayerBullets().get(i).draw(batch);
			} else if (bulletPool.getPlayerBullets().get(i).isSpawned()) {
				bulletPool.removePlayerBullet(bulletPool.getPlayerBullets().get(i));
				i--;
			}
		}
		//endregion

		//region handle drawing of any weapons
		for(Item item: weaponPool.getItems()) {
			if (item.getClass().equals(WeaponSprite.class)) {
				((WeaponSprite) item).draw(batch);
			}
		}
		//endregion

		//region handle drawing of any enemies
		for(int i = 0; i < enemyPool.getEnemySprites().size(); i++) {
			enemyPool.getEnemySprites().get(i).draw(batch);
		}
		//endregion

		if (isAttacking && !isDashing) {
			attackConstantly();
		}

		batch.end();

		fontBatch.begin();

		font.draw(fontBatch, "" + Gdx.graphics.getFramesPerSecond(), 20, 20);

		//region handle drawing of any enemy text
		for(int i = 0; i < enemyPool.getEnemySprites().size(); i++) {
			Vector3 enemyText = getScreenPosFromGameWorld(enemyPool.getEnemySprites().get(i).getX(), enemyPool.getEnemySprites().get(i).getY());
			font.draw(fontBatch,
				enemyPool.getEnemySprites().get(i).getStats().getHealth() + "/" + enemyPool.getEnemySprites().get(i).getStats().getMaxHealth(),
				enemyText.x, enemyText.y);
		}
		//endregion

		//region text hit detection

		//hit detection for rendering 'press e to pick up weapon'
		for (Item item: weaponPool.getItems()) {
			if (item.collision(playerCharacter.getBoundingRectangle())) {
				Vector3 position = this.getScreenPosFromGameWorld(((WeaponSprite) item).getX(), ((WeaponSprite) item).getY());
				font.draw(fontBatch, "Push e to pick up", position.x, position.y);
			}
		}
		//endregion

		fontBatch.end();

		//region hit detection here
		//hit detection for player bullets
		for (int i = 0; i < bulletPool.getPlayerBullets().size(); i++) {
			for (EnemySprite enemySprite : enemyPool.getEnemySprites()) {
				if (bulletPool.getPlayerBullets().get(i).collision(enemySprite.getBoundingRectangle())) {
					enemySprite.getStats().damage(bulletPool.getPlayerBullets().get(i).getDamage());
					bulletPool.getPlayerBullets().get(i).kill(bulletPool);
					i--;
				}
			}
		}
		//endregion

		//region check for deaths and calculate them
		for (int i = 0; i < enemyPool.getEnemySprites().size(); i++) {
			if (enemyPool.getEnemySprites().get(i).isDead()) {
				enemyPool.getEnemySprites().remove(i);
				i--;
			}
		}
		//endregion

		//region debug drawings - leave this here, doesn't need cleanup
		DebugRenderer.DrawDebugLine(meleeAttack.center, meleeAttack.center.cpy().add(meleeAttack.start), camera.combined);
		DebugRenderer.DrawDebugLine(meleeAttack.center, meleeAttack.center.cpy().add(meleeAttack.end), camera.combined);
		for (EnemySprite enemy : enemyPool.getEnemySprites())
			DebugRenderer.DrawDebugBox(enemy.getBoundingRectangle(), camera.combined);

		DebugRenderer.DrawDebugBox(playerCharacter.getBoundingRectangle(), camera.combined);
		//endregion

		timeSinceLastAttackSeconds += delta;
	}

	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		font.dispose();
		fontBatch.dispose();
		assetManager.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {

		if (keycode == Input.Keys.UP
			&& direction.y >= 0)
			direction.y = 1;
		if (keycode == Input.Keys.DOWN
			&& direction.y <= 0)
			direction.y = -1;
		if (keycode == Input.Keys.LEFT
			&& direction.x <= 0)
			direction.x = -1;
		if (keycode == Input.Keys.RIGHT
			&& direction.x >= 0)
			direction.x = 1;
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {

		if(keycode == Input.Keys.UP && direction.y > 0) {
			if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
				direction.y = -1;
			} else {
				direction.y = 0;
			}
		}
		if(keycode == Input.Keys.DOWN && direction.y < 0)
			if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
				direction.y = 1;
			} else {
				direction.y = 0;
			}
		if(keycode == Input.Keys.LEFT && direction.x < 0)
			if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				direction.x = 1;
			} else {
				direction.x = 0;
			}
		if(keycode == Input.Keys.RIGHT && direction.x > 0)
			if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
				direction.x = -1;
			} else {
				direction.x = 0;
			}

		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		//when the e key is typed (down AND up) then the weapon underneath you will be picked up. if there are multiple
		//it'll default to the first one in the weaponPool array
		if (character == 'e') {
			for (int i = 0; i < weaponPool.getItems().size(); i++) {
				Item item = weaponPool.getItems().get(i);
				if (item.collision(playerCharacter.getBoundingRectangle())) {
					playerCharacter.getWeaponSprite().dropWeapon(playerCharacter.getX(), playerCharacter.getY(), direction);
					weaponPool.addItemSprite(playerCharacter.getWeaponSprite());
					item.applyItem(playerCharacter);
					weaponPool.getItems().remove(item);
					return true;
				}
			}
		}
		//when the space key is pressed, start dashing - dashing will always take place over a set time, distance will vary
		if (character == ' ') {
			isDashing = true;
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (button == 0) {
			isAttacking = true;
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (button == 0) {
			isAttacking = false;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
