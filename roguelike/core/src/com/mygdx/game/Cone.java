package com.mygdx.game;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Cone {

    Vector2 start, end;
    float radius;
    Vector2 center;

    public Cone(float radius, Vector2 center, float fov, float playerRotation) {
        this.center = center;
        this.radius = radius;

        start = new Vector2(0, radius);
        start.rotate(playerRotation).rotate(-fov/2);
        end = new Vector2(0, radius);
        end.rotate(playerRotation).rotate(fov/2);
    }

    public void update(float radius, Vector2 center, float fov, float playerRotation) {
        this.center = center;
        this.radius = radius;

        start.set(0, radius);
        start.rotate(playerRotation).rotate(-fov/2);
        end.set(0, radius);
        end.rotate(playerRotation).rotate(fov/2);
    }

    //this is going to be used once i switch the collision detection from rectangles to polygons
    public boolean collision(Rectangle rect) {

        if (rect.contains(center))
            return true;

        Array<Vector2> points = getPolygonVertices(rectangleToPolygon(rect));

        for (Vector2 point : points){
            Vector2 relativePoint = new Vector2(point.x - center.x, point.y - center.y);

            if (!areClockwise(start, relativePoint)
                && areClockwise(end, relativePoint)
                && withinRadius(relativePoint, radius))
                return true;
        }

        return false;
    }

    public Polygon rectangleToPolygon(Rectangle rect) {
        float[] points = {rect.x, rect.y,
            rect.x + rect.width, rect.y,
            rect.x + rect.width, rect.y + rect.height,
            rect.x, rect.y + rect.height};
        return new Polygon(points);
    }

    Array<Vector2> getPolygonVertices(Polygon polygon) {
        float[] vertices = polygon.getTransformedVertices();

        Array<Vector2> result = new Array<>();
        for (int i = 0; i < vertices.length/2; i++) {
            float x = vertices[i * 2];
            float y = vertices[i * 2 + 1];
            result.add(new Vector2(x, y));
        }
        return result;
    }

    private boolean areClockwise(Vector2 v1, Vector2 v2) {
        return -v1.x*v2.y + v1.y*v2.x > 0;
    }

    private boolean withinRadius(Vector2 v2, float radius) {
        return v2.x*v2.x + v2.y*v2.y <= radius*radius;
    }
}
