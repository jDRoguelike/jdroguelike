package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import model.Player;

public interface Item {

    String getName();
    void setName(String name);
    boolean collision(Rectangle boundingBox);
    void applyItem(PlayerSprite player);
}