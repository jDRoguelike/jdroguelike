package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Map {
    private boolean[][] walkable = new boolean[25][25];
    private Sprite mapSprite;
    int tileX, tileY, tileX2, tileY2;

    public Map(Sprite mapSprite) {
        this.mapSprite = mapSprite;
    }

    public void with(String[] map) {
        for (int i = 0; i < map.length; i++) {
            char[] line = map[i].toCharArray();
            for (int j = 0; j < line.length; j++) {
                walkable[i][j] = line[j] != '0';
            }
        }
    }

    public Vector2 isWalkable(Vector2 velocity, Vector2 position, float width, float height) {

        int tileSize = 5;
        Vector2 newPos = position.cpy();
        newPos.add(velocity.x, 0);
        tileX = (int) (newPos.x) / tileSize;
        tileY = (int) (newPos.y) / tileSize;
        tileX2 = (int) (newPos.x + width) / tileSize;
        tileY2 = (int) (newPos.y + height) / tileSize;

        //if we cannot walk in the X direction set our velocity to 0
        if (!(walkable[tileX][tileY] && walkable[tileX][tileY2]
            && walkable[tileX2][tileY] && walkable[tileX2][tileY2]))
            velocity.x = 0;

        newPos.x = position.x;
        newPos.add(0, velocity.y);

        tileX = (int) (newPos.x) / tileSize;
        tileY = (int) (newPos.y) / tileSize;
        tileX2 = (int) (newPos.x + width) / tileSize;
        tileY2 = (int) (newPos.y + height) / tileSize;

        //if we can walk in the Y direction return the velocity as the X is already sorted
        if (walkable[tileX][tileY] && walkable[tileX][tileY2]
            && walkable[tileX2][tileY] && walkable[tileX2][tileY2])
            return velocity;
        else
            velocity.y = 0;

        return velocity;
    }

    public boolean collision(Vector2 velocity, Vector2 position, float width, float height) {

        int tileSize = 5;
        Vector2 newPos = position.cpy();
        newPos.add(velocity);
        tileX = (int) (newPos.x) / tileSize;
        tileY = (int) (newPos.y) / tileSize;
        tileX2 = (int) (newPos.x + width) / tileSize;
        tileY2 = (int) (newPos.y + height) / tileSize;

        //check the collision to the object
        return !(walkable[tileX][tileY] && walkable[tileX][tileY2]
            && walkable[tileX2][tileY] && walkable[tileX2][tileY2]);
    }

    public Sprite getMapSprite() {
        return mapSprite;
    }

    public void setMapSprite(Sprite mapSprite) {
        this.mapSprite = mapSprite;
    }
}
