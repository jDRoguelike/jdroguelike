package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public interface BulletObject {

    void setSpawned(boolean spawned);

    boolean isSpawned();

    void setTexture(Texture texture);

    Texture getTexture();

    void setBulletSprite(Sprite bulletSprite);

    Sprite getBulletSprite();

    void setDirection(Vector2 direction);

    Vector2 getDirection();

    void setSpeed(int speed);

    int getSpeed();

    void setMaxDistance(int maxDistance);

    int getMaxDistance();

    void setDamage(int damage);

    int getDamage();

    void kill(BulletPool pool);

    void update(float delta, Map map);

    void draw(SpriteBatch sb);

    boolean collision(Rectangle boundingBox);
}