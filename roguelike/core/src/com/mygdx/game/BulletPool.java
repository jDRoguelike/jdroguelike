package com.mygdx.game;

import java.util.ArrayList;

public class BulletPool {

    private ArrayList<PlayerBullet> playerBullets;
    private ArrayList<PlayerBullet> enemyBullets;

    public BulletPool() {
        playerBullets = new ArrayList<>();
        enemyBullets = new ArrayList<>();
    }

    public void addPlayerBullet(float px, float py, PlayerSprite player) {
        PlayerBullet playerBullet = new PlayerBullet(player.getX() + player.getWidth() / 2, player.getY() + player.getHeight() / 2,
            px, py, player.getWeaponSprite().getPlayerAttackSprite(), player.getStats());
        playerBullets.add(playerBullet);
        playerBullets.get(playerBullets.size() - 1).setSpawned(true);
    }

    public void removePlayerBullet(PlayerBullet playerBullet) {
        playerBullet.setSpawned(false);
        playerBullets.remove(playerBullet);
    }

    public ArrayList<PlayerBullet> getPlayerBullets() {
        return playerBullets;
    }

    public ArrayList<PlayerBullet> getEnemyBullets() {
        return enemyBullets;
    }
}
