package com.mygdx.game;

import java.util.ArrayList;

public class ItemPool {

    private ArrayList<Item> items;

    public ItemPool() {
        items = new ArrayList<>();
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public void addItemSprite(Item item) {
        items.add(item);
    }

    public void removeItemSprite(Item item) {
        items.remove(item);
    }
}
