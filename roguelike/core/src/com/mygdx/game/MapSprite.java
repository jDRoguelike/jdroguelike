package com.mygdx.game;

public class MapSprite {

    int[][] whichTile;
    boolean[][] walkable;

    MapSprite(int[][] whichTile, boolean[][] walkable) {
        this.whichTile = whichTile;
        this.walkable = walkable;
    }

    public int[][] getWhichTile() {
        return whichTile;
    }

    public void setWhichTile(int[][] whichTile) {
        this.whichTile = whichTile;
    }

    public boolean[][] getWalkable() {
        return walkable;
    }

    public void setWalkable(boolean[][] walkable) {
        this.walkable = walkable;
    }
}
