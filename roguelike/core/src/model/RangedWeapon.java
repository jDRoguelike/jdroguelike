package model;

public class RangedWeapon extends Weapon {

    public RangedWeapon(String name, int levelDrop, int damage, float atkspeed, int flightspeed, int flightDistance) {
        this.setName(name);
        this.setLevelDrop(levelDrop);
        this.setDamage(damage);
        this.setAtkspeed(atkspeed);
        this.setFlightSpeed(flightspeed);
        this.setFlightDistance(flightDistance);
    }

    //region getters and setters
    public int getFlightSpeed() {
        return flightSpeed;
    }

    public void setFlightSpeed(int flightSpeed) {
        this.flightSpeed = flightSpeed;
    }

    public int getFlightDistance() {
        return flightDistance;
    }

    public void setFlightDistance(int flightDistance) {
        this.flightDistance = flightDistance;
    }
    // endregion

    private int flightSpeed;
    private int flightDistance;
}