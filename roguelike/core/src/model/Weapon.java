package model;

public abstract class Weapon {

    /*
    levelDrop: the level this weapon drops at
    damage: the damage the weapon deals
    atkspeed: the atkspeed of the weapon
    element: the element of the weapon
     */

    private String name;
    private int levelDrop;
    private int damage;
    private float atkspeed;

    public float getAtkspeed() {
        return atkspeed;
    }

    public void setAtkspeed(float atkspeed) {
        this.atkspeed = atkspeed;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getLevelDrop() {
        return levelDrop;
    }

    public void setLevelDrop(int levelDrop) {
        this.levelDrop = levelDrop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
