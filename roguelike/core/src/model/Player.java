package model;

import java.util.List;

public class Player extends Unit {

    public Player(String name, int health, int level, int armour, int power, int speed) {
        super(name, health, level, armour, power, speed);
    }

    // region getters and setters
    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
    // endregion

    private int dexterity;
    private List<Item> items;
}