package model;

public class MeleeWeapon extends Weapon {

    public MeleeWeapon(String name, int levelDrop, int damage, float atkspeed, int fieldOfAttack, float attackDistance, float attackTime) {
        this.setName(name);
        this.setLevelDrop(levelDrop);
        this.setDamage(damage);
        this.setAtkspeed(atkspeed);
        this.setFieldOfAttack(fieldOfAttack);
        this.setAttackDistance(attackDistance);
        this.setAttackTime(attackTime);
    }

    private int fieldOfAttack;
    private float attackDistance;
    private float attackTime;

    public int getFieldOfAttack() {
        return fieldOfAttack;
    }

    public void setFieldOfAttack(int fieldOfAttack) {
        this.fieldOfAttack = fieldOfAttack;
    }

    public float getAttackDistance() {
        return attackDistance;
    }

    public void setAttackDistance(float attackDistance) {
        this.attackDistance = attackDistance;
    }

    public float getAttackTime() {
        return attackTime;
    }

    public void setAttackTime(float attackTime) {
        this.attackTime = attackTime;
    }
}
