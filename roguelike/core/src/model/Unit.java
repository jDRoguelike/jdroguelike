package model;

public abstract class Unit {
    // region getters and setters

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() { return maxHealth; }

    public void setMaxHealth(int maxHealth) { this.maxHealth = maxHealth; }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getArmour() {
        return armour;
    }

    public void setArmour(int armour) {
        this.armour = armour;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // endregion

    private int health;
    private int maxHealth;
    private Weapon weapon;
    /*
    level: determines the multiplier for stats and drops
    armour: determines how much damage mitigation a unit has
    power: determines how much damage a unit deals
    speed: determines how fast the unit moves
     */
    private int level;
    private int armour;
    private int power;
    private int speed;
    private String name;

    Unit(String name, int health, int level, int armour, int power, int speed) {
        this.setName(name);
        this.setHealth(health);
        this.setMaxHealth(health);
        this.setLevel(level);
        this.setArmour(armour * this.getLevel());
        this.setPower(power * this.getLevel());
        this.setSpeed(speed * this.getLevel());
    }

    public void damage(int value) {
        int afterModifier = value - this.getArmour();
        if (afterModifier < 1) afterModifier = 1;
        this.setHealth(this.getHealth() - afterModifier);
    }

    public int determineDamageOutput() {
        return this.getWeapon().getDamage() * this.getPower();
    }
}
