package model;

public class Boss extends Unit {

    public Boss(String name, int health, int level, int armour, int power, int speed) {
        super(name, health, level, armour, power, speed);
    }
}
