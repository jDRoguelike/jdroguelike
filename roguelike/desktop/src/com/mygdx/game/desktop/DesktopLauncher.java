package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.Roguelike;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.fullscreen = false;
		config.width = 1920;
		config.height = 1080;
		config.vSyncEnabled = true; // Setting to false disables vertical sync
		config.foregroundFPS = 0;
		config.backgroundFPS = 0;
		new LwjglApplication(new Roguelike(), config);
	}
}